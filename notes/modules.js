var cartModule = (function () {
    var x = 1;
    function placki() { };

    return {
        addToCart: placki,
        remove: function () { }
    };
})()
// placki == undefined

var cart = cartModule()

cart.addToCart()
cart.remove()
/*  ==== */
// cartModulejs
define(function () {
    var x = 1;
    return {
        addToCart: function () { },
        remove: function () { }
    };
})()


require(['/cartModule.js'], function (module) {

    module.addToCart()
    module.remove()
})
/*
    AMD - Asynchronous Module Definition
    Require.js -  Loader, AMD optimizer

    optimize.exe -i main.js -o bundle.js
*/

 <script module="/cartModule.mjs"></script> 