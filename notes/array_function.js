// myArray.forEach = function forEach(array, callback) {
//     for (var i in array) {
//         callaback(array[i], i, array)
//     }
// } 

products.forEach(function (product) {
    console.log('hello', product.name)
});


products.forEach(function (product, index, all) {
    console.log('hello', product.name, index, all)

    if (index == all.length - 1) {
        console.log('Zapisz sie na newsletter!')
    }
});


function iterator(product, index, all) {
    console.log('hello', product.name, index, all)
}

products.forEach(iterator)

/* === */

var prodList = [1, 2, 3, 4, 5]

prodList.map(x => Math.pow(x, 2))
// [1, 4, 9, 16, 25]

prodList.filter(function (x, i) {
    return x % 2 != 0
})
//  [1, 3, 5]

/* === */

prodList.reduce(function (sum, x, i) {
    sum += x;
    console.log(x, sum)
    return sum;

}, 0)


/* === */

prodList.reduce(function (acc, x, i) {
    acc[x % 2 == 0 ? 'even' : 'odd'] += x;
    return acc;

}, { even: 0, odd: 0 })

// {even: 6, odd: 9}
// even: 6
// odd: 9

prodList.reduce(function (acc, x, i) {
    acc[x % 2 == 0 ? 'even' : 'odd'].push(x);
    return acc;

}, { even: [], odd: [] })

// {even: Array(2), odd: Array(3)}
// even: (2) [2, 4]
// odd: (3) [1, 3, 5]
// __proto__: Object


/* === */

prodList.every(function (x) { x > 0 })
// false
prodList.every(function (x) { return x > 0 })
// true
prodList.every(function (x) { return x > 1 })
// false
prodList.some(function (x) { return x > 1 })
// true
prodList.some(function (x) { return x < 1 })
// false



var prodList = [1, 2, 3, 4, 5]

isEven = function (x) { return x % 2 == 0 };

prodList.filter(isEven)
// (2) [2, 4]


/* === */

var not = function (f) {
    return function (item) { return !f(item) }
}

var isOdd = not(isEven)

prodList.filter(isOdd);

// Wiecej funkcji:
// https://underscorejs.org/
// https://lodash.com/
// https://ramdajs.com/

