function makePerson(name) {

    return {
        sayHello: function () { console.log('I am' + name) }
    }
}

var alice = makePerson('Alice')
var bob = makePerson('Bob')

alice.sayHello()
// VM6158:4 I amAlice

bob.sayHello()
// VM6158:4 I amBob

function makePerson(name) {

    return {
        name: name,
        sayHello: function () { console.log('I am ' + name) }
    }
}

function makePerson(name) {

    var self = {
        name: name,
        sayHello: function () { console.log('I am ' + self.name) }
    }
    return self;
}

function WhatIsThis() {
    console.log(this)
}

WhatIsThis()
// VM6564:2 Window {postMessage: ƒ, blur: ƒ, focus: ƒ, close: ƒ, parent: Window, …}

var name = 'placki'

window.name
// "placki"

window.WhatIsThis()
// VM6564:2 Window {postMessage: ƒ, blur: ƒ, focus: ƒ, close: ƒ, parent: Window, …}

var obj = {
    f: WhatIsThis
}

obj.f()
// VM6564:2 {f: ƒ}

obj.name = 'jestem obj!'
// "jestem obj!"
obj.f()
// VM6564:2 {f: ƒ, name: "jestem obj!"}

var obj2 = {
    f: WhatIsThis
}

obj2.f()
// VM6564:2 {f: ƒ}

/* == THIS == */

new WhatIsThis()
// VM6564:2 WhatIsThis {}

var newObj = new WhatIsThis()
// VM6564:2 WhatIsThis {}__proto__: constructor: ƒ WhatIsThis()__proto__: Object

newObj instanceof WhatIsThis
// true



/* === */

function Person(name) {
    this.name = name;
}

Person('placki')
// 
window.name
// "placki"
var obj = { Person: Person }

obj.Person('alice')
// 
obj
// {Person: ƒ, name: "alice"}
Person.apply(obj, ['placki'])

obj
// {Person: ƒ, name: "placki"}
Person.apply({}, ['placki'])

new Person('Bob')
// Person {name: "Bob"}

/* ========== */

function sayHello() {
    console.log(this.name);
}

function Person(name) {

    this.name = name;

    this.sayHello = sayHello

}

var alice = new Person('Alice');
var bob = new Person('Bob');

alice.sayHello == bob.sayHello
true

alice.sayHello()
// VM7804: 2 Alice

bob.sayHello()
// VM7804: 2 Bob
/* ========== */

function Person(name) {
    this.name = name;
}

Person.prototype = {
    sayHello: function sayHello() {
        console.log(this.name);
    }
}

var alice = new Person('Alice');
var bob = new Person('Bob');

alice.__proto__.sayHello == bob.__proto__.sayHello
true

alice.__proto__ == bob.__proto__
true

/* ========== */

function Person(name) {
    this.name = name;
}

Person.prototype = {
    sayHello: function sayHello() {
        console.log(this.name);
    }
}

var alice = new Person('Alice');
var bob = new Person('Bob');

function Employee(name, salary) {
    Person.apply(this, [name]) // super(name)
    this.salary = salary;
}

Employee.prototype = Object.create(Person.prototype);
Employee.prototype.work = function () {
    console.log('I want my ' + this.salary);
};

var tom = new Employee('Tom', 1400);

tom.work()
// VM8551: 22 I want my 1400
// 
tom.sayHello()
// VM8551: 8 Tom

tom
// Employee { name: "Tom", salary: 1400 }
// name: "Tom"
// salary: 1400
// __proto__:
// work: ƒ()
// __proto__:
// sayHello: ƒ sayHello()

/* EcmaScript 2015 Class */

class Person {

    constructor(name) { this.name = name }

    sayHello() { console.log(this.name) }
}

class Employee extends Person {

    constructor(name, salary) {
        super(name);
        this.salary = salary
    }

    work() {
        console.log('Salary ' + this.salary)
    }

}

var tom = new Employee('Tom', 1500);


// Employee { name: "Tom", salary: 1500 }
//  name: "Tom"
//  salary: 1500
//      __proto__: Person
//          constructor: class Employee
//          work: ƒ work()
//          __proto__:
//              constructor: class Person
//              sayHello: ƒ sayHello()

var div = document.createElement('div')
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__



tim.sayHello = function () { return 'Go away!' }
// ƒ(){ return 'Go away!' }
tim
// Employee { name: "Tim", salary: 15000, sayHello: ƒ } 
//  name: "Tim"
//  salary: 15000
//  sayHello: ƒ()
//      __proto__: Personconstructor: class Employee
//          work: ƒ work()
//          __proto__: constructor: class Person
//              sayHello: ƒ sayHello()__proto__: Object

tim.sayHello()
// "Go away!"