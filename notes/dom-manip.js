// document.querySelector -> El
// document.querySelectorAll -> [El,El,...]

// innerText
// textContent
// innerHTML
// style
// id
// ...

$('.js-product-details').innerText = '<b>ala ma kota</b>'
"<b>ala ma kota</b>"

$('.js-product-details').innerHTML = '<b>ala ma kota</b>'



var p = document.querySelector('.list-group-item:nth-child(1)')
function clickOnce() {
    console.log('You only see this once!', this)
    p.removeEventListener('dblclick', clickOnce)
}
p.addEventListener('dblclick', clickOnce)


/* === DATASET === */
// <div data-id="123" data-type="product" >JavaScript</div>
elem.dataset

// DOMStringMap {id: "123", type: "product"}
// id: "123"
// type: "product"
// __proto__: DOMStringMap




// var w = window.open('file:///C:/Users/Student/Desktop/Public/szkolenie-javascript/index.html','_blank',{ width: 300 })