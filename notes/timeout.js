
function zrobToPozniej() {
    console.log('pozniej')
}

setTimeout(zrobToPozniej, 2 * 1000)
1
// VM127:3 pozniej

function zrobToPozniej() {
    console.log('pozniej')
}

setInterval(zrobToPozniej, 2 * 1000)
2
// 16VM136:3 pozniej
clearTimeout(1)

// 7VM136:3 pozniej
clearInterval(2)

/* === */
function zrobToPozniej() {
    console.log('pozniej')
}

var h = setInterval(zrobToPozniej, 2 * 1000)

clearTimeout(h)

/* === */

function zrobToPozniej() {
    console.log(2);
}

console.log(1);

setTimeout(zrobToPozniej, 0)

console.log(3);
// VM304:6 1
// VM304:10 3
// VM304:3 2

/* === */

var start = new Date();
var now = new Date();
var i = 0;

function zrobToPozniej() {
    now = new Date();

    console.log(now - start, i += 100)
}

var h = setInterval(zrobToPozniej, 100)

// Moment.js 
// https://momentjs.com/  - "20 minut temu"