
```js
var xhr = new XMLHttpRequest()
xhr.onerror = console.log
xhr.onreadystatechange = console.log
xhr.open('GET','data/products.json')

xhr.onload = function(event){
	console.log(event)
	console.log( JSON.parse( xhr.response ) )
}
xhr.send()

```
Promise

```js
var p = new Promise(function(handler){
	setTimeout( function(){
		handler('placki')
    } , 3000)
});

// wyswietl
p.then(console.log)

// zaznacz
p.then(console.log)

// zapamietaj
p.then(console.log)


```

```js
var req = fetch('data/products.json');

req.then(function(resp){
	console.log('response')
    resp.json()
		.then(console.log)
})
```

```js

function echo(text, cb){
	setTimeout(function(){
		cb(text)
    },1000)
}

echo('placki', function(p){
    echo('Lubie '+p, function(p){
        echo('Bardzo '+p, function(p){
			
			console.log(p) 
        })
    })
})

```

```js

function echo(text){
	return new Promise(function(resolve){
        
		setTimeout(function(){
            resolve(text)
        },1000)

	})
}

echo('Placki')
.then(function(r){
    echo('Lubie '+r)
        .then(function(r){
            console.log(r)
        })
})

var promise = echo('Placki')
.then(function(r){
	return echo('Lubie '+r)
})
.then(function(r){
   console.log(r)
})

[1,2,3]
.map(function(x){ return x * 2 })
.map(function(x){ return x + 2 })
.filter(function(x){ return x %2 ==0})
.forEach(console.log)

```

```js
var req = fetch('data/products.json');

req.then(function(resp){
    return resp.json()		
})
.then(console.log)

```

```js
// https://jquery.com/download/

$.getJSON('data/products.json').then(console.log)
```


```js
var loading;
window.addEventListener('scroll', function(){
var rect = $('.list-group').get(0).getBoundingClientRect();

console.log(window.scrollY,	rect.bottom);

    if(rect.bottom < 0){
        clearTimeout(loading)
        loading = setTimeout(function(){
            refreshProducts()
        },500)
    }
});
document.body.style.height='99999px'

```

https://jsonplaceholder.typicode.com/

https://github.com/typicode/json-server

npm install json-server -g 

json-server .\data.json --watch

// Sending POST data:

```js
fetch('https://httpbin.org/post', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({a: 1, b: 'Textual content'})
  });

$.ajax({
  type: "POST",
  url: url,
  data: data,
//   success: success,
//   dataType: 'json'
}).then(console.log)

$.post(url, data).then(console.log)


```