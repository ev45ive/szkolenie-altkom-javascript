
var products = [{
    id: 123,
    name: 'JavaScript',
    price: 95.90,
    promotion: true,
}, {
    id: 234,
    name: 'HTML',
    price: 65.90,
    promotion: true
}, {
    id: 345,
    name: 'CSS',
    price: 55.90,
    promotion: true
}]

function createCart() {
    this._cart = {
        items: [],
        total: 0
    }

    this.getItems = function () { return this._cart.items }

    this.getTotal = function () { return this._cart.total }

    this.add = function (product, amount) {
        // Jeśli nie podano ilości
        if (amount === undefined) {
            amount = 1;
        }

        // Sprawdź czy nie ma już w koszyku
        var foundItems = this._cart.items.filter(function (item) {
            return item.product.id === product.id
        })
        var found = foundItems.pop()

        // Jeśli jest to zwieksz ilość
        if (found) {
            found.amount += amount
        } else {
            // Jeśli nie ma to dodaj nowy
            this._cart.items.push({
                product: product,
                amount: amount
            })
        }
        this.calculateTotal()
    }

    this.remove = function (product, amount) {
        // Jeśli nie podano ilości
        if (amount === undefined) {
            amount = 1;
        }

        this._cart.items = this._cart.items.filter(function (item) {
            // Pozostałe elementy przepisujemy bez zmian
            if (item.product.id !== product.id) {
                return true;
            }
            item.amount -= amount;

            // Pomijamy puste elementy
            if (item.amount <= 0) {
                return false
            }
            // Zostawiamy zmieniony
            return true
        })

        this.calculateTotal()
    }

    this.render = function () {
        console.log('=== Shopping Cart ===')

        this._cart.items.forEach(function (item) {
            var product = item.product;
            var amount = item.amount;
            var subtotal = product.price * amount

            console.log(
                product.name + ' ' + product.price + ' x ' + amount + ' = ' + subtotal)
        })

        console.log('===')
        console.log('Total', this._cart.total)
    }

    this.calculateTotal = function () {

        this._cart.total = this._cart.items.reduce(function (sum, item) {

            sum += item.amount * item.product.price

            return sum
        }, 0);

        return this._cart.total
    }
}

var myCart = new createCart()
myCart.add(products[1], 2)
myCart.add(products[0], 1)
console.log(myCart.render())

var myCart2 = new createCart()
myCart2.add(products[1], 2)
myCart2.add(products[0], 1)
console.log(myCart2.render())


function ProductsList(products) {
    this.products = products

    this.render = function () {

        this.products.forEach(function (currentProduct, index) {
            var position = index + 1 + '. ';
            console.log(position + this.renderProduct(currentProduct))
        }.bind(this))
    }

    this.renderProduct = function (product) {
        var name = product.name,
            price = product.price.toFixed(2),
            promotion = true,
            promotion = (product.promotion ? ' PROMOTION' : '');

        return name + ' - ' + price + promotion;
    }
}

// var prodList = new ProductsList(favouriteProducts)
// var prodList = new ProductsList(myProducts)
// var prodList = new ProductsList(searchProducts)

var prodList = new ProductsList(products)
prodList.render()



