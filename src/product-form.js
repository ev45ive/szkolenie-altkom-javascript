
// $('.js-product-form')

var formElem = document.querySelector('.js-product-form');

// $(formElem).find('.js-add-product') 
var addButton = formElem.querySelector('.js-add-product')
var updateButton = formElem.querySelector('.js-update-product')


updateButton.addEventListener('click', function (event) {
    var id = formElem.elements['id']
    var name = formElem.elements['name']
    var price = formElem.elements['price']
    var promotion = formElem.elements['promotion']

    if (!name.value || !price.value /* || !promotion */) {
        window.alert('Brakujace pola')
        return;
    }
    var product = {
        id: id.value,
        name: name.value,
        price: price.value,
        promotion: promotion.checked
    }

    updateProduct(product)
        .then(function () {
            debugger
            return refreshProducts()
        })
        .then(function () {
            
            return selectProduct(product)
        })
})

addButton.addEventListener('click', function (event) {
    var name = formElem.elements['name']
    var price = formElem.elements['price']
    var promotion = formElem.elements['promotion']

    if (!name.value || !price.value /* || !promotion */) {
        window.alert('Brakujace pola')
        return;
    }
    var product = {
        name: name.value,
        price: price.value,
        promotion: promotion.checked
    }

    createProduct(product)
        .then(function (data) {
            // console.log(data)
            refreshProducts()
            name.value = ''
            price.value = ''
            promotion.checked = false
        })

})

function updateProductForm(product) {

    var id = formElem.elements['id']
    var name = formElem.elements['name']
    var price = formElem.elements['price']
    var promotion = formElem.elements['promotion']

    id.value = product.id
    name.value = product.name
    price.value = product.price
    price.value = product.price
    promotion.checked = product.promotion

    var update = formElem.querySelector('.js-update-product')
    var add = formElem.querySelector('.js-add-product')

    if (product.id) {
        update.style.display = 'inline'
        add.style.display = 'none'
    } else {
        update.style.display = 'none'
        add.style.display = 'inline'
    }
}

function createProduct(product) {
    return fetch('http://localhost:3000/products/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(product)
    })
        .then(function (response) { return response.json() })
}

function removeProduct(product) {
    console.log('delete', product)

    fetch('http://localhost:3000/products/' + product.id, {
        method: "DELETE"
    }).then(function () {
        refreshProducts()
        selectProduct({})
    })
}

function updateProduct(product) {
    return fetch('http://localhost:3000/products/' + product.id, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(product)
    }).then(function (resp) { return resp.json() })
}


    // $.post('http://localhost:3000/products/', JSON.stringify({
    //     name: name,
    //     price: price,
    //     promotion: promotion
    // }))
    //     .then(function (data) {
    //         console.log(data)
    //     })