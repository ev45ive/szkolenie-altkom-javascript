
/* Clock */
var clockElem = document.querySelector('.js-clock')

setInterval(function () {
    var d = new Date();

    var hours = d.getHours() > 9 ? d.getHours() : ('0' + d.getHours());
    clockElem.innerText = hours + ':' + d.getMinutes() + ':' + d.getSeconds();

    // clockElem.innerText = d.toLocaleTimeString();
}, 1000)