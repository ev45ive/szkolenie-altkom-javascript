// Document Object Model
var products = []

refreshProducts().then(function (products) {
    selectProduct()
    addProductListeners(products)
})

function refreshProducts() {

    var request = $.getJSON('http://localhost:3000/products/')

    return request.then(function (data) {
        renderProducts(data)
        products = data;
        return products
    })
}

function renderProductItem(product) {
    // listItemTpl = ...
    var p = $('<div>' + product.name +
        '<span class="close">&times;</span>' +
        '</div>');

    p.attr({
        'data-id': product.id,
        'data-type': 'product',
        class: 'list-group-item'
    })
    return p[0]
}


function renderProducts(products) {
    // Znajdź element <div id="root">
    var productListElem = document.querySelector('.js-products-list');
    productListElem.innerHTML = ''

    // Dla każdego produktu utwórz i dodaj element z nazwą produktu
    products.forEach(function (product) {
        var elem = renderProductItem(product)
        productListElem.appendChild(elem)
    })
}


function addProductListeners(products) {
    var list = document.querySelector('.list-group')

    list.addEventListener('click', function (event) {
        var item = event.target.closest('.list-group-item')
        var id = item.dataset['id']
        var product = findProduct(products, id);

        if (event.target.matches('.close')) {
            removeProduct(product)
        }

        if (event.target.matches('.list-group-item')) {
            selectProduct(product)
        }
    })
}


function findProduct(products, id) {
    const items = products.filter(function (p) { return p.id == id })
    return items.pop()
}

function renderProduct(product) {
    var elem = document.querySelector('.js-product-details')
    var productTpl = document.getElementById('productTpl')

    elem.innerHTML = productTpl.textContent;

    elem.querySelector('.product-name')
        .innerText = product.name;

    elem.querySelector('.product-price')
        .innerText = product.price;

    elem.querySelector('.product-promotion')
        .innerText = product.promotion;
}

function selectProduct(product) {
    if (!product) {
        product = { name: '', price: 0, promotion: false }
    }
    renderProduct(product)
    updateProductForm(product)

    var list = document.querySelector('.list-group')
    var items = list.querySelectorAll('.list-group-item')
    items.forEach(function (item) {
        var id = item.dataset['id']

        if (id == product.id) {
            item.classList.add('active')
        } else {
            item.classList.remove('active')
        }
    })
    // var item = items.querySelector('[data-id=' + product.id + ']')
    // item.classList.add('active')
}



// document.body.addEventListener('mousemove',function(event){
//     console.log(event.x, event.y)
//     findProduct(event.x)
// })



// $(event.target).closest('.list-group-item')
// event.target.closest('.list-group-item')
// function findParent(event, selector) {
//     var parent = event.target;
//     while (parent && !parent.match(selector)) {
//         parent = parent.parentElement
//     };
//     return parent;
// }